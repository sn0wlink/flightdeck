<p align="center">
  <img src="https://gitlab.com/sn0wlink/flightdeck/-/raw/main/assets/gitlab-readme.png" alt="Flight Deck Logo"/>
</p>

# Flight Deck

Created by: David Collins-Cubitt
Date: Feb 2023

# Description
A tablet friendly web framework for rapid development of front end interfaces.

# Languages
Flight Deck is written in PHP, HTML, CSS with no future depency for mysql.

# Design Guidelines / Rules
- 4 spaces indentation
- Camel case function / variable names
- Keep it simple design philosophy
- Keep code tidy and readable
- I reserve the right to refuse code merges (This project is a dictatorship)

# Code of conduct
Be excellent to each other.

