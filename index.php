<?php

/*
*	Flight Deck
*
*	David Collins-Cubitt
*	www.sn0wlink.com
*
*	A tablet based web framework for rapid development 
*	of interface front ends.
*/

// Included files
include ('config.php');

// Load core modules
$CoreFiles = glob("core/*.php");
foreach ($CoreFiles as $Filename) {
    include ($Filename);
}

// EOF