<?php

include ('./config.php');

// Setup Session
session_start();
$_SESSION['LOGIN'] = FALSE;

if ($_SESSION['LOGIN']) {
    echo 'You\'re Logged In';
}

// Load array
echo $USER_LIST;

// EOF